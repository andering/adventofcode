#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import os, sys


def main():
    filepath = os.path.join(os.path.dirname(__file__),sys.argv[1])

    if not os.path.isfile(filepath):
        print("File path {} does not exist. Exiting...".format(filepath))
        sys.exit()

    with open(filepath) as fp:
        ints = [int(_) for _ in fp.read().split(',')]


    def compute(ints, a, b):
        ints[1] = a
        ints[2] = b

        for i in range(0, len(ints), 4):
            op, a, b, c = ints[i : i + 4]
            if op == 1:
                ints[c] = ints[a] + ints[b]
            elif op == 2:
                ints[c] = ints[a] * ints[b]
            elif op == 99:
                break;
            else:
                raise Exception('Monty Python, we have a problem')

        return ints[0]

    #part1
    print(compute(ints,12,2))

    #part2
    for a in range(100):
        for b in range(100):
            output = compute(list(ints), a, b)
            if output == 19690720:
                print(100 * a + b)




if __name__ == '__main__':
    main()
