#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import os, sys


def main():
    filepath = os.path.join(os.path.dirname(__file__),sys.argv[1])

    if not os.path.isfile(filepath):
        print("File path {} does not exist. Exiting...".format(filepath))
        sys.exit()

    with open(filepath) as fp:
        mass = [int(line) for line in fp.readlines()]

    #part1
    print(sum([max(line // 3 - 2,0) for line in mass]));

    #part2
    total = 0
    while sum(mass):
        mass = [max(line // 3 - 2,0) for line in mass]
        total += sum(mass)

    print(total)

if __name__ == '__main__':
    main()
